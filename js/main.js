(function() {
  var canvas = document.getElementById("mainCanvas");

  // make canvas 100% of body
  canvas.width = document.body.clientWidth;
  canvas.height = document.body.clientHeight;

  var WIDTH = canvas.width;
  var HEIGHT = canvas.height;

  var ctx = canvas.getContext('2d');

  // game variables
  var planets = [];
  var goal;
  var player = {
    x: 0,
    y: Math.random() * HEIGHT,
    speedX: 3,
    speedY: 0,
    acc: 1
  };

  // reset the player to starting position
  player.reset = (function(x, y) {
    return function() {
      player.x = x;
      player.y = y;
      player.speedX = 3;
      player.speedY = 0;
      player.acc = 1;
      waitForStart();
      draw();
    };
  }(player.x, player.y));

  function Planet(init) {
    init = init || {};
    this.radius = init.radius || Math.random() * 40 + 20;
    this.x = init.x || Math.random() * (WIDTH - this.radius * 2);
    this.y = init.y || Math.random() * (HEIGHT - this.radius * 2);
    this.movable = init.movable || false;
    this.hittable = init.hittable || true;
    this.rotatesAround = init.rotatesAround || null;
  }


  Planet.prototype.getDistance = function() {
    var a = this.x - player.x;
    var b = this.y - player.y;
    var c = a * a + b * b;
    if (c > 0) {
      return c - this.radius / 2;
    }
    return c + this.radius / 2;
  };

  Planet.prototype.startDrag = function() {
    if (!this.movable) {
      return;
    }
    var self = this;
    window.onmousemove = function(e) {
      self.x = e.clientX;
      self.y = e.clientY;
      draw();
    };

    window.onmouseup = function() {
      window.onmousemove = null;
    };
  };

  Planet.prototype.isHit = function() {
    if (!this.hittable) {
      return;
    }
    if (
      this.x + this.radius > player.x &&
      this.x - this.radius < player.x &&
      this.y + this.radius > player.y &&
      this.y - this.radius < player.y) {

      return true;
    }
    return false;
  };

  Planet.prototype.move = function () {
    if (!this.rotatesAround) {
      return;
    }

  };

  function makeWorld() {
    goal = new Planet();
    planets = [goal];
    numberOfPlanets = Math.random() * 5 + 1;
    while (planets.length <= numberOfPlanets) {
      planets.push(new Planet({
        movable: true
      }));
    }
  }

  function update() {
    var i,
      planet;

    move();
    draw();

    // if borders are passed
    if (player.x < 0 ||
      player.x > WIDTH ||
      player.y < 0 ||
      player.y > HEIGHT) {
      player.reset();
      return;
    }

    // if the goal is hit
    if (goal.isHit()) {
      console.log('won!');
      moveCanvas();
      return;
    }

    // if another planet is hit
    for (i = planets.length - 1; i >= 0; i--) {
      planet = planets[i];
      if (planet.isHit()) {
        player.reset();
        return;
      }
    }

    window.requestAnimationFrame(update);
  }

  function move() {
    var i,
      planet,
      dragX = 0,
      dragY = 0,
      distance;

    for (i = planets.length - 1; i >= 0; i--) {
      planet = planets[i];
      planet.move();
      distance = planet.getDistance();
      dragX += 10 * planet.radius * (planet.x - player.x) / distance;
      dragY += 10 * planet.radius * (planet.y - player.y) / distance;
    }
    player.speedX = (player.speedX * player.acc) + dragX / 60;
    player.speedY = (player.speedY * player.acc) + dragY / 60;
    player.x += player.speedX;
    player.y += player.speedY;
  }

  function moveCanvas(){
     var i,
      planet,
      dragX = 0,
      dragY = 0,
      distance;

    for (i = planets.length - 1; i >= 0; i--) {
      planets[i].x -= 15;
      if(planets[i].x < -planets[i].radius){
        delete planets[i++];
      }
    }

    player.x -= 15;

    if (goal.x < 50) {
      waitForStart();
    }else{
      window.requestAnimationFrame(moveCanvas);
    }
    draw();
  }

  function draw() {
    ctx.fillStyle = '#000';
    ctx.fillRect(0, 0, WIDTH, HEIGHT);

    ctx.fillStyle = '#ff4';
    ctx.beginPath();
    for (x = planets.length - 1; x >= 0; x--) {
      planet = planets[x];
      ctx.arc(planet.x, planet.y, planet.radius, 0, 2 * Math.PI);
    }
    ctx.fill();
    ctx.closePath();
    ctx.beginPath();
    ctx.fillStyle = '#35f';
    ctx.arc(goal.x, goal.y, goal.radius, 0, 2 * Math.PI);
    ctx.fill();
    ctx.closePath();
    ctx.fillStyle = '#4f4';
    ctx.fillRect(player.x, player.y, 10, 10);
  }

  var handleStart = function handleStart() {
    window.requestAnimationFrame(update);
    window.onkeydown = null;
    canvas.removeEventListener("touchstart", handleStart, false);
  };

  function waitForStart() {
    canvas.addEventListener("touchstart", handleStart, false);
    window.onkeydown = handleStart;
  }

  window.onmousedown = function(e) {
    var i,
      planet;
    for (i = planets.length - 1; i >= 0; i--) {
      planet = planets[i];
      if (
        planet.x + planet.radius > e.clientX &&
        planet.x - planet.radius < e.clientX &&
        planet.y + planet.radius > e.clientY &&
        planet.y - planet.radius < e.clientY) {

        planet.startDrag();
        return;
      }
    }
  };


  makeWorld();
  draw();
  waitForStart();

}());
